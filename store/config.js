export const state = () => ({
  config: null,
  statusCode: 0,
})

export const getters = {
  getConfig: ({config}) => config,
  getConfigStatus: ({statusCode}) => statusCode,
  homeUrl: ({config}) => config.home,
  verCache: ({config}) => config.persistor_key,
  allowedPostTypes: ({config}) => config.post_types,
  backendUrl: ({config}) => config.siteurl + '/wp-admin',
  getOption: ({config}, getters, contex, rootState) => (optionName, lang) => {
    if (optionName && getters.getConfig) {
      /* const currentLangauge = lang || rootState.getLocale
      if (
        getters.getConfig.options &&
        getters.getConfig.options.translations &&
        getters.getConfig.options.translations[currentLangauge]
      ) {
        return getters.getConfig.options.translations[currentLangauge][
          optionName
        ]
      } */
    }
    return null
  },
  getHomeUrl: (state) => (state.config ? state.config.home : '')
}

export const mutations = {
  SET_CONFIG(state, payload) {
    state.config = payload.data
    state.statusCode = payload.statusCode
  }
}

export const actions = {
  /*   loadConfig({ commit, error }) {
    commit('SET_CONFIG', { data: process.env.wpConfig, statusCode: 200 })
  } */
  // Since we're uisng nuxt-i18 - config is from nuxt.config.js
  async loadConfig({ commit }) {
    const now = new Date().getTime()
    const base = process.env.NODE_ENV
/*     console.log( base ) */

    return await this.$axios.$get('https://backend.jirosan.pl/wp-content/themes/backend/api/cached/config.json?cache='+ now).then((res) => {
      console.log(res)
      if(res){
        commit('SET_CONFIG', {
          data: res,
          statusCode: 200
        })
        return res
      }else{
        commit('SET_CONFIG', {
          data: false,
          statusCode: 404
        })
      }
    })
  }
}